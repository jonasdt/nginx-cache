<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


function purge($uri) {
    $path = rtrim($uri, '/');
    $host = 'nginx-cache.app';
    $file = '/home/vagrant/cache/' . $host . $path. '.html';

    if (file_exists($file)) {
        unlink($file);
    }
}

Route::get('/78/welcome', ['middleware' => ['html-cache'], function () {
    return view('welcome', ['title' => '', 'uri' => '/78/welcome']);
}]);

Route::get('/78/welcome/purge', function () {
    purge('/78/welcome');
    return view('welcome', ['title' => 'purge', 'uri' => '/78/welcome']);
});

Route::get('/78/welcome/1', ['middleware' => ['html-cache'], function () {
    return view('welcome', ['title' => '', 'uri' => '/78/welcome/1']);
}]);

Route::get('/78/welcome/1/purge', function () {
    purge('/78/welcome/1');
    return view('welcome', ['title' => 'purge', 'uri' => '/78/welcome/1']);
});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    //
});
