<?php

namespace App\Http\Middleware;

use Closure;

class HtmlCacheMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request);
    }

    /**
     * Perform any final actions for the request lifecycle.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Http\Response $response
     * @return void
     */
    public function terminate($request, $response)
    {
        $path = rtrim((str_replace('/purge', '', $request->getPathInfo())), '/');
        $host = $request->getHttpHost();
        $file = '/home/vagrant/cache/' . $host . $path. '.html';
        $folder = dirname($file);

        if (!file_exists($folder)) {
            mkdir($folder, 0777, true);
        }

        file_put_contents($file, $response->getContent());
    }
}
